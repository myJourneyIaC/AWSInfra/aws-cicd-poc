resource "aws_key_pair" "ssh-key" {
  key_name   = var._ssh_key_name_
  public_key = var._ssh_public_key_
}
