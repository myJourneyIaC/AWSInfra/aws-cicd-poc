
variable "_instance_type_" {
  type = string
  description = "Type of instance"
  default = "t2.medium"
}

variable "_ssh_key_name_" {
  type = string
  description = "SSH Public Key Name"
}

variable "_private_key_path_" {
  type = string
  description = "SSH Private Key Path"
}

variable "_user_" {
  type = string
  description = "User that will connect with the instance"
}

variable "_sg-acl-id_" {
  type = string
  description = "Security Group ACL ID"
}

variable "_sg-attach-id_" {
  type = string
  description = "Additional Security Group ACL ID"
}

variable "_tag_env_" {
  type = string
  description = "Environment Tags"
}

variable "_tag_name_" {
  type = string
  description = "Tags for ECS Name"
}

variable "_tag_cicd_name_" {
  type = string
  description = "Tags for CICD Deploy"
  default = "MyCICDPoc"
}
variable "_tag_ansible_" {
  type = string
  description = "Name of the role that will be execute"
}

variable "_working_dir_" {
  type = string
  description = "Directory Path of the ansible roles that will be execute"
}

variable "_iam_instance_profile_" {
  type = string
  description = "IAM Instance Profile" 
}
