resource "aws_instance" "ami-instance" {
  ami             = data.aws_ami.aws.id
  instance_type   = var._instance_type_

  key_name        = var._ssh_key_name_

  iam_instance_profile = var._iam_instance_profile_

  security_groups = [var._sg-acl-id_]

  associate_public_ip_address = "true"

  tags = {
    Environment = "dev"
    Name = "minik8s-devops"
    MyCICDPoc = ""
  }

  provisioner "remote-exec" {
    inline = [ 
               "sudo yum install -y epel-release ",
               "sudo yum install -y python3-pip git ruby unzip",
               "sudo pip3 install docker mysql-connector-python redis",
               "pip3 install awscli --upgrade --user"
             ]

    connection {
      agent       = "false"
      host        = self.public_ip
      type        = "ssh"
      user        = var._user_
      private_key = file(var._private_key_path_)
    }
  }

  provisioner "local-exec" {
    working_dir = var._working_dir_
    command = "ansible-playbook -u '${var._user_}' -i '${self.public_ip},' --private-key '${var._private_key_path_}' customs.yml --tags '${var._tag_ansible_}'  -T 3600"
  }
}
