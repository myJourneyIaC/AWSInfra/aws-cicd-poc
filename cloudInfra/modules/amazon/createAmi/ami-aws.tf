
data "aws_ami" "aws" {
  most_recent = true

  filter {
    name   = "name"
    values = ["*.20181112-x86_64-ebs"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["591542846629"] # AWS
}
