
resource "aws_instance" "ami-instance" {
  ami             = data.aws_ami.ubuntu.id
  instance_type   = var._instance_type_

  key_name        = var._ssh_key_name_

  security_groups = [var._sg-acl-id_, var._sg-attach-id_]

  associate_public_ip_address = "true"

  tags = {
    Name = var._tag_name_
  }

  provisioner "remote-exec" {
    inline = [ 
      "sudo apt-get update",
      "sudo apt install -y software-properties-common",
      "sudo apt-get -y install python3-paramiko"
    ]

    connection {
      agent       = "false"
      host        = self.public_ip
      type        = "ssh"
      user        = var._user_
      private_key = file(var._private_key_path_)
    }
  }

  provisioner "local-exec" {
    working_dir = var._working_dir_
    command = "ansible-playbook -u '${var._user_}' -i '${self.public_ip},' --private-key '${var._private_key_path_}' customs.yml --tags '${var._tag_ansible_}'  -T 3600"
  }

}

#resource "aws_network_interface_sg_attachment" "sg_attach" {
#  security_group_id    = var._sg-attach-id_
#  network_interface_id = aws_instance.ami-instance.primary_network_interface_id 
#}
