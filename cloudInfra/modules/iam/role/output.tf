output "ec2role_name" {
  value = aws_iam_role.ec2role.name
}

output "ec2role_arn" {
  value = aws_iam_role.ec2role.arn
}

output "ec2profile_name" {
  value = aws_iam_instance_profile.ec2Profile.name
}
