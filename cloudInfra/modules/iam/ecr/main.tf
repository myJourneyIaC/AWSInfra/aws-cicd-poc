
//EC2 Container Registry (ecr)
//ECRFullService

resource "aws_ecr_repository" "ecrfullservice" {
  count = length(var.ecr_repo_name)
  name  = element(var.ecr_repo_name,count.index)
}

resource "aws_iam_policy" "ecrfullservice" {
    name        = var.ecrfullservice_ecr_service_name
    path        = "/"
    policy      = file("../files/iam/ecr/ECRFullDevOpsService.json")

#      lifecycle {
#        prevent_destroy = true
#      }
}

resource "aws_iam_role_policy_attachment" "ec2role-policy-ecr" {
  role          = var._ec2role_name_
  policy_arn    = aws_iam_policy.ecrfullservice.arn
}
