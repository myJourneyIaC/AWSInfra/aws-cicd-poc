
variable "_ec2role_name_" {
  type = string
  description = "EC2 Role Name from which will attach those policies"
}

variable "_ec2role_arn_" {
  type = string
  description = "EC2 Role ARN from which will attach those policies"
}

variable "_cicd_project_name_" {
  type = string
  description = "Name of the project"

}

variable "_ec2_target_id_" {
  type = string
  description = "Ec2 ID on Which we are attach the ELB on this project"

}

variable "_ecs_container_name_" {
  type = string
  description = "ECS Container Name"

}


variable "_elb_subnet_id_" {
  type = list
  description = "Sub Net"
}

variable "_elb_vpc_id_" {
  type = string
  description = "VPC ID"
}

variable "_git_repository_name_" {
  type = string
  description = "GitHub Repository Name"
}

variable "_git_repository_description_" {
  type = string
  description = "GitHub Description Repository"
}

variable "_git_owner_name_" {
  type = string
  description = "GitHub Owner Name"
}

variable "_git_oauth_token_" {
  type = string
  description = "GitHub OAuth Token"
}

variable "_git_secret_webhook_" {
  type = string
  description = "GitHub/GitLab Web Hook"
}

variable "_codebuild_policy_name_" {
  type = string
  description = "CodeBuild Policy Name"
}

variable "_codepipeline_policy_name_" {
  type = string
  description = "Codepipeline Policy Name"
}

variable "_s3_policy_name_" {
  type = string
  description = "S3 Policy Name"
}

variable "_codedeploy_config_name_" {
  type = string
  description = "Codedeploy Configuration Name"
  default = "MyCodePipelineDemo"
}

variable "_codedeploy_computer_platform_" {
  type = string
  description = "Codedeploy Compute Platform"
}

variable "_ecs_policy_name_" {
  type = string
  description = "ECS Policy Name"
}
