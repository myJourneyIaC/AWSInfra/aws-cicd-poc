### Policy Attach 
resource "aws_iam_role_policy_attachment" "ec2role-policy-codedeploy-service" {
  role          = var._ec2role_name_
  policy_arn    = "arn:aws:iam::aws:policy/AWSCodeDeployRoleForECS"
}

resource "aws_iam_role_policy_attachment" "ec2role-policy-codedeploy" {
  role          = var._ec2role_name_
  policy_arn    = "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforAWSCodeDeploy"
}

resource "aws_iam_role_policy_attachment" "ec2role-policy-codepipeline" {
  role          = var._ec2role_name_
  policy_arn    = "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforDataPipelineRole"
}

resource "aws_iam_role_policy_attachment" "ec2role-policy-ecs-autoscaling" {
  role          = var._ec2role_name_
  policy_arn    = "arn:aws:iam::aws:policy/AutoScalingFullAccess"
}

### GitHub Repo ##############
resource "github_repository" "git-repo" {
  name        = var._git_repository_name_
  description = var._git_repository_description_
}

resource "github_repository_webhook" "cicd-webhook" {
  repository      = github_repository.git-repo.name

  configuration {
    url          = aws_codepipeline_webhook.cicd-webhook.url
    content_type = "json"
    insecure_ssl = true
    secret = var._git_secret_webhook_
  }

  events = ["push"]
}

######### Bucket S3 ################

resource "aws_s3_bucket" "codepipeline_bucket" {
  bucket = "cicd-poc-bucket"
  acl    = "private"

  tags = {
    Name        = "My CICD Bucket"
    Environment = "Dev"
  }
}

resource "aws_iam_policy" "codepipeline_policy" {
  name   = var._codepipeline_policy_name_
  path   = "/"
  policy = file("../files/iam/cicd/CodePipelinePolicy.json")
}

resource "aws_iam_role_policy_attachment" "ec2role-policy-ecr" {
  role          = var._ec2role_name_
  policy_arn    = aws_iam_policy.codepipeline_policy.arn
}

######### Code Pipeline ###############

resource "aws_codepipeline" "cicd-codepipeline" {
  name     = var._cicd_project_name_
  role_arn = var._ec2role_arn_

  artifact_store {
    location = aws_s3_bucket.codepipeline_bucket.bucket
    type     = "S3"

  }

  stage {
    name = "Source"

    action {
      name             = "Source"
      category         = "Source"
      owner            = "ThirdParty"
      provider         = "GitHub"
      version          = "1"
      output_artifacts = ["source_output"]

      configuration = {
        Owner  = var._git_owner_name_
        Repo   = github_repository.git-repo.name
        Branch = "master"
        OAuthToken = var._git_oauth_token_
      }
    }
  }
  stage {
    name = "Build"

    action {
      name             = "Build"
      category         = "Build"
      owner            = "AWS"
      provider         = "CodeBuild"
      input_artifacts  = ["source_output"]
      output_artifacts = ["build_output"]
      version          = "1"

      configuration = {
        ProjectName = var._cicd_project_name_
      }
    }
  }
  stage {
    name = "Deploy"

    action {
      name            = "Deploy"
      category        = "Deploy"
      owner           = "AWS"
      provider        = "CodeDeploy"
      version         = "1"
      input_artifacts  = [ "source_output" ]

      configuration = {
        ApplicationName     = aws_codedeploy_app.cicd-codedeploy.name
        DeploymentGroupName = var._codedeploy_config_name_
      }
    }
  }
}

##### Code Pipeline Webhook #######

resource "aws_codepipeline_webhook" "cicd-webhook" {
  name            = "cicd-webhook-github"
  authentication  = "GITHUB_HMAC"
  target_action   = "Source"
  target_pipeline = aws_codepipeline.cicd-codepipeline.name

  authentication_configuration {
    secret_token =  var._git_secret_webhook_
  }

  filter {
    json_path    = "$.ref"
    match_equals = "refs/heads/{Branch}"
  }
}

##### Code Build ################

resource "aws_iam_policy" "codebuild_policy" {
  name   = var._codebuild_policy_name_
  path   = "/"
  policy = file("../files/iam/cicd/CodeBuildPolicy.json")
}

resource "aws_iam_role_policy_attachment" "ec2role-policy-codebuild" {
  role          = var._ec2role_name_
  policy_arn    = aws_iam_policy.codebuild_policy.arn
}

resource "aws_codebuild_project" "cicd-codebuild" {
  name          = var._cicd_project_name_
  description   = "cicd_codebuild_project"
  build_timeout = "5"
  service_role  = var._ec2role_arn_

  artifacts {
    type = "NO_ARTIFACTS"
  }

  cache {
    type     = "S3"
    location = aws_s3_bucket.codepipeline_bucket.bucket
  }

  environment {
    compute_type                = "BUILD_GENERAL1_SMALL"
    image                       = "aws/codebuild/standard:2.0"
    type                        = "LINUX_CONTAINER"
    image_pull_credentials_type = "CODEBUILD"

    environment_variable {
      name  = "Environment"
      value = "dev"
    }
    privileged_mode = true

  }

  logs_config {
    cloudwatch_logs {
      group_name = "log-group"
      stream_name = "log-stream"
    }

  }

  source {
    type            = "GITHUB"
    location        =  github_repository.git-repo.http_clone_url
    git_clone_depth = 1

    git_submodules_config {
        fetch_submodules = true
    }
  }

  source_version = "master"

  tags = {
    Environment = "dev"
  }

}

##### ELB #######

resource "aws_lb" "cicd-lb" {
  name               = "cicd-lb-tf"
  internal           = false
  load_balancer_type = "network"
  subnets            = var._elb_subnet_id_

  enable_deletion_protection = false

  tags = {
    Environment = "dev"
  }
}

resource "aws_lb_target_group" "cicd-lb-tg" {
  name        = "cicd-lb-tg"
  port        = 80
  protocol    = "TCP_UDP"
  vpc_id      = var._elb_vpc_id_
  depends_on = [aws_lb.cicd-lb]
}

resource "aws_lb_target_group" "cicd-lb-green-tg" {
  name        = "cicd-lb-green-tg"
  port        = 80
  protocol    = "TCP_UDP"
  vpc_id      = var._elb_vpc_id_
  depends_on = [aws_lb.cicd-lb]
}

resource "aws_lb_listener" "cicd-lb-listener" {  
  load_balancer_arn = aws_lb.cicd-lb.arn  
  port              = 80
  protocol          = "TCP_UDP"
  
  default_action {    
    target_group_arn = aws_lb_target_group.cicd-lb-tg.arn
    type             = "forward"  
  }
}

##### CodeDeploy #######

resource "aws_codedeploy_app" "cicd-codedeploy" {
  compute_platform = var._codedeploy_computer_platform_
  name             = github_repository.git-repo.name
}

resource "aws_codedeploy_deployment_group" "cicd-codedeploy-group" {
  app_name               = aws_codedeploy_app.cicd-codedeploy.name
  deployment_group_name  = var._codedeploy_config_name_
  service_role_arn       = var._ec2role_arn_
  deployment_config_name = "CodeDeployDefault.AllAtOnce"

  deployment_style {
    deployment_option = "WITHOUT_TRAFFIC_CONTROL"
    deployment_type   = "IN_PLACE"
  }

    ec2_tag_set {
    ec2_tag_filter {
      key   = "MyCICDPoc"
      type  = "KEY_ONLY"
    }

  }

  load_balancer_info {

    target_group_info {
        name = aws_lb_target_group.cicd-lb-tg.name
     } 

    target_group_pair_info {
      prod_traffic_route {
        listener_arns = [aws_lb_listener.cicd-lb-listener.arn]
      }

      target_group {
        name = aws_lb_target_group.cicd-lb-tg.name
      }

      target_group {
        name = aws_lb_target_group.cicd-lb-green-tg.name
      }
    }
  }

  auto_rollback_configuration {
    enabled = false
    events  = ["DEPLOYMENT_FAILURE"]
  }

}
