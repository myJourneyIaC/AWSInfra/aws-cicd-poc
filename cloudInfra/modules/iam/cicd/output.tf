output "cicd-s3-bucket" {
  value = aws_s3_bucket.codepipeline_bucket.arn
}

output "cicd-repo" {
  value = github_repository.git-repo.http_clone_url
}
