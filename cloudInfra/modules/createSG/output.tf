
output "sg" {
  value = "${aws_security_group.ingress-all.name}"
}

output "id" {
  value = "${aws_security_group.ingress-all.id}"
}
