output "ec2role_name" {
  value = module.ec2role.ec2role_name
}

output "cicd-s3-bucket" {
  value = module.ec2-cicd.cicd-s3-bucket
}

output "cicd-repo" {
  value = module.ec2-cicd.cicd-repo
}

output "ec2role_arn" {
  value = module.ec2role.ec2role_arn
}

output "ec2profile_name" {
  value = module.ec2role.ec2profile_name
}

output "my_ami_minik8s_public_ip" {
  value = module.ami-k8s.my-public-ipaddr
}
