

module "sg-ssh" {

   source         = "../modules/createSG"

   _vpc_id_       = "vpc-7487801c"
   _sg_name_      = "sshd-sg"
   _tcp_port_     = "22" 

}

module "sg-redis" {

   source         = "../modules/createSG"

   _vpc_id_       = "vpc-7487801c"
   _sg_name_      = "redis-sg"
   _tcp_port_     = "6379" 

}


module "sg-mysql" {

   source         = "../modules/createSG"

   _vpc_id_       = "vpc-7487801c"
   _sg_name_      = "mysql-sg"
   _tcp_port_     = "3306" 

}

module "sg-http" {

   source         = "../modules/createSG"

   _vpc_id_       = "vpc-7487801c"
   _sg_name_      = "http-sg"
   _tcp_port_     = "8080"

}
