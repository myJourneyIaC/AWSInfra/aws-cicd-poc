
variable "_ssh_pub_" {
  type = string
  description = "SSH Key"
}

variable "_ssh_priv_path_" {
  type = string
  description = "SSH Key"
}

variable "_tf_ec2role_profile_name_" {
  type = string 
}

variable "_tf_ec2role_name_" {
  type = string 
}

variable "_tf_ecr_repo_name" {
  type = list
}

variable "_tf_ecrfullservice_ecr_service_name" {
  type = string 
}

variable "_tf_git_owner_name_" {
  type = string 
}

variable "_tf_git_repository_name_" {
  type = string 
}

variable "_tf_git_repository_description_" {
  type = string 
}

variable "_tf_git_secret_webhook_" {
  type = string 
  description = "GitHub Webhook" 
}

variable "_tf_git_oauth_token_" {
  type = string 
  description = "GitHub Token" 
}

variable "_tf_cicd_project_name_" {
  type = string 
}

variable "_tf_ecs_container_name_" {
  type = string 
}

variable "_tf_s3_policy_name_" {
  type = string 
}

variable "_tf_codebuild_policy_name_" {
  type = string 
}

variable "_tf_codepipeline_policy_name_" {
  type = string 
}

variable "_tf_codedeploy_computer_platform_" {
  type = string 
}

variable "_tf_codedeploy_config_name_" {
  type = string 
}

variable "_tf_ecs_policy_name_" {
  type = string 
}

variable "_tf_elb_subnet_id_" {
  type = list 
}

variable "_tf_elb_vpc_id_" {
  type = string 
}

variable "_tf_tag_env_" {
  type = string 
}

variable "_tf_tag_name_" {
  type = string 
}

variable "_tf_user_" {
  type = string 
}

variable "_tf_private_key_path_" {
  type = string 
}

variable "_tf_tag_ansible_" {
  type = string 
}

variable "_tf_working_dir_" {
  type = string 
}

