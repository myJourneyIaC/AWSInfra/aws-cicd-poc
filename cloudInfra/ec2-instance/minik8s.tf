
module "new-ssh-key" {

   source         = "../modules/createSSHKey"

   _ssh_key_name_     =  "devops-ssh-key"
   _ssh_public_key_   =  var._ssh_pub_

}

module "ec2role" {

   source = "../modules/iam/role"

   _ec2role_name_ = var._tf_ec2role_name_
   _ec2role_profile_name_ = var._tf_ec2role_profile_name_  
}

module "ecr_poc" {

   source = "../modules/iam/ecr"

   ecr_repo_name = var._tf_ecr_repo_name 

   ecrfullservice_ecr_service_name   = var._tf_ecrfullservice_ecr_service_name 
   _ec2role_name_                    =  module.ec2role.ec2role_name
}

module "ec2-cicd" {

   source                         =  "../modules/iam/cicd"

   _ec2role_name_                 =  module.ec2role.ec2role_name
   _ec2role_arn_                  =  module.ec2role.ec2role_arn
   _git_owner_name_               =  var._tf_git_owner_name_ 
   _git_repository_name_          =  var._tf_git_repository_name_         
   _git_repository_description_   =  var._tf_git_repository_description_  
   _git_secret_webhook_           =  var._tf_git_secret_webhook_    
   _git_oauth_token_              =  var._tf_git_oauth_token_       
   _cicd_project_name_            =  var._tf_cicd_project_name_            
                                                                    
   _ecs_container_name_           =  var._tf_ecs_container_name_           
                                                                    
   _s3_policy_name_               =  var._tf_s3_policy_name_               
   _codebuild_policy_name_        =  var._tf_codebuild_policy_name_        
   _codepipeline_policy_name_     =  var._tf_codepipeline_policy_name_     
                                                                    
   _codedeploy_computer_platform_ =  var._tf_codedeploy_computer_platform_ 
   _codedeploy_config_name_       =  var._tf_codedeploy_config_name_       
   _ecs_policy_name_              =  var._tf_ecs_policy_name_              
   _elb_subnet_id_                =  var._tf_elb_subnet_id_
   _elb_vpc_id_                   =  var._tf_elb_vpc_id_ 
   _ec2_target_id_                =  module.ami-k8s.ec2id

}

module "ami-k8s" {

   source                 =  "../modules/amazon/createAmi"

   _ssh_key_name_         =  module.new-ssh-key.key_name
   _sg-acl-id_            =  module.sg-ssh.sg
   _sg-attach-id_         =  module.sg-http.sg
   _iam_instance_profile_ =  module.ec2role.ec2profile_name
   _private_key_path_     =  var._tf_private_key_path_ 
   _tag_env_              =  var._tf_tag_env_     
   _tag_name_             =  var._tf_tag_name_    
   _user_                 =  var._tf_user_        
   _tag_ansible_          =  var._tf_tag_ansible_ 
   _working_dir_          =  var._tf_working_dir_ 

}
